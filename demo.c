#include <GL/gl.h>
#include <SDL/SDL.h>

#define WIDTH 800
#define HEIGHT 600

void mixaudio(void *unused, unsigned char *stream, int len)
{
  static int t = 0;
  int i;
  for (i = 0; i < len; i++, t++)
    stream[i] = (t * 42000*t & t >> 5); // Magic goes here
}

void _start()
{
  SDL_AudioSpec fmt =
  {
    .freq = 4096,
    .format = AUDIO_S16,
    .channels = 1,
    .samples = 512,
    .callback = mixaudio,
    .userdata = 0
  };
  SDL_OpenAudio(&fmt, NULL);

  SDL_SetVideoMode(WIDTH, HEIGHT, 0, SDL_OPENGL);

  SDL_PauseAudio(0);
  unsigned int etime, last_time, time = SDL_GetTicks();
  unsigned int last_tick = time;
  float angle = 0;

#ifdef DEBUG
  unsigned int last_frame = time, fps=0;
  SDL_Event event;
  int done = 0;
  while (!done)
#else
    while (time/1000 < 5) // Animation will last 5 seconds
#endif
    {
      last_time = time;
      time = SDL_GetTicks();
      etime = time - last_time;
#ifdef DEBUG
      fps++;
      if (time - last_frame >= 1000)
      {
        printf("fps: %d\n", fps);
        fps = 0;
        last_frame = time;
      }
#endif
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    angle += etime/1000.0;
    if (time - last_tick >= 1000)
    {
      glRotatef(10, 0, 0, -1);
      last_tick = time;
    }

    glBegin(GL_TRIANGLES);
    glColor3f(0,0,0);
    glVertex2f(-0.03, 0);
    glVertex2f(0.03, 0);
    glColor3f(1,1,1);
    glVertex2f(0, 0.8);
    glEnd();

    SDL_GL_SwapBuffers();

#ifdef DEBUG
    SDL_PollEvent(&event);
    if (event.type == SDL_KEYDOWN)
    {
      switch (event.key.keysym.sym)
      {
      case SDLK_q:      done = 1; break;
      case SDLK_ESCAPE: done = 1; break;
      }
    }
#endif

  }

  SDL_CloseAudio();

  __asm__ ( \
      "movl $1,%eax\n"
      "xor %ebx,%ebx\n"
      "int $0x80\n"
      );
}
