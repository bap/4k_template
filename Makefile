NAME = demo
TMP  = $(NAME).unpacked

CC ?= gcc

CFLAGS  = -std=c99 -ffast-math -Os -s -fno-inline -fexpensive-optimizations
ASFLAGS =
LDFLAGS = -nostdlib -lc -lSDL -lGL

CSRC  = demo.c
ASSRC =
OBJ   = $(CSRC:.c=.o) $(ASSRC:.S=.o)

.PHONY: all clean distclean

all: compress

compress: $(NAME)
	sstrip $(NAME)
	mv $(NAME) $(TMP)
	cp unpack.header $(NAME)
	gzip -cn9 $(TMP) >> $(NAME)
	chmod +x $(NAME)
	wc -c $(NAME)

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) $(CSRC) -o $(NAME) $(LDFLAGS)

clean:
	rm -f $(OBJ) $(TMP)

distclean: clean
	rm -f $(NAME)
